<?php

namespace App\Domain\Api\Request;

use App\Http\Requests\ApiRequest;
use Illuminate\Validation\Rule;

class RegisterRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
        return [
            'name' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email'),
            ],
            'password' => [
                'required',
                'min:6',
                'string',
            ],
            'phone' => [
                'required',
                'min:8',
                'max:14',
            ],
        ];
    }

    /**
     * @return array
     */
    public function persist()
    {
        return array_merge($this->only('name', 'email','phone'), [
            'password' => bcrypt($this->get('password')),
            'verification_code' => time() . str_random(40)
        ]);
    }
}