<?php

namespace App\Domain\Api\Request;

use App\Http\Requests\ApiRequest;
use Illuminate\Support\Facades\Auth;

class LoadingRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->route()->parameters());
        return [
            'item_details_id' => 'required',
            'date' => 'required',
            'quantity' => 'required',
            'gate_pass' => 'required',
            'car_number' => 'required',
            'driver_name' => 'required', 
        ];
    }

    public function persist()
    { 
        return array_merge(
            $this->only('item_details_id','date','quantity','gate_pass','car_number','driver_name')
        );  
    }
}