<?php

namespace App\Domain\Api\Request;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ApiRequest;

class SubItemsRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->route()->parameters());
        return [
            'item_id' => 'required',
            'name' =>'required',
        ];
    }

    public function persist()
    { 
        //dd($this->get('title'));
        return array_merge(
            $this->only('item_id','name'),['user_id' => Auth::user()->id]
        );  
    }
}