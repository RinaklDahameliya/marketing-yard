<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\StoreItemsRequest;
use App\Models\Items;
use App\Domain\Api\Request\ItemListCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Http\Resources\ItemIdCollection;

class StoreItemsController extends Controller
{
    public function store(StoreItemsRequest $request)
    {
    	$items = Items::create($request->persist());
    
    	if(!empty($items)){
    		return response()->json([
	            'success' => true,
	            'message' => 'Item Added Successfully.',
                'data' => [
                    'id' => $items->getKey(),
                    'name' => $items->name,
                ],
	        ]);
    	}
    	return response()->json([
	            'success' => false,
	            'message' => 'Something went wrong.',
	        ]);
    }

    public function update(StoreItemsRequest $request,$id)
    {
    	$items = Items::where('id',$id)->update($request->persist());

    	if(!empty($items)){
    		return response()->json([
	            'success' => true,
	            'message' => 'Item Edit Successfully.',
	        ]);
    	}
    	return response()->json([
	            'success' => false,
	            'message' => 'Something went wrong.',
	        ]);
    }
    public function destroy($id)
    {
    	$items = Items::find($id)->delete();
		if(!empty($items)){
    		return response()->json([
	            'success' => true,
	            'message' => 'Item Delect Successfully.',
	        ]);
    	}
    	return response()->json([
	            'success' => false,
	            'message' => 'Something went wrong.',
	        ]);    	
    }

}
