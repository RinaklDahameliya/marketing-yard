<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Domain\Api\Request\RegisterRequest;
use App\Notifications\UserVerification;
use App\Http\Resources\UserResource;
use Symfony\Component\HttpFoundation\JsonResponse;

class RegisterController extends Controller
{
     public function register(RegisterRequest $request)
    {  // dd($request->all());
        $user = User::create($request->persist());
        $user->notify(new UserVerification());
        return new UserResource($user);
    }

    public function verifyAccount($code)
    {
        $user = User::where('verification_code', $code)->first();

        if (!$user) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Your link has already been used, or has expired.'
            ]);
        }

        $user->fill([
            'verification_code' => null,
            'is_verified' => true
        ])->save();

        return view('front.verified');
    }

}