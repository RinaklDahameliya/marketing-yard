<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Domain\Api\Request\ItemDetailsRequest;
use App\Models\ItemDetails;
use App\Http\Resources\DateWiseItemViewDetailsCollection;
use Illuminate\Support\Facades\Auth;

class DateWiseItemViewDetailsController extends Controller
{
    public function show(Request $request)
    {
		if(isset($request->date) && !empty($request->date)){
			$items_date = ItemDetails::where('date', $request->date)->where('user_id', Auth::user()->id)->get();
			return response()->json([
                'success' => true,
                'message' => 'Item View Details Successfully.',
                'Item' => new DateWiseItemViewDetailsCollection($items_date)
            ]);
		}else{
			$items = ItemDetails::query()->where('user_id', Auth::user()->id)->get();
			return response()->json([
                'success' => true,
                'message' => 'Item View Details Successfully.',
                'Item' => new DateWiseItemViewDetailsCollection($items)
            ]);
		}
        
		
    }
}