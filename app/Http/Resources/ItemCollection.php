<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\ItemDetails; 
use App\Models\Items; 
use App\Models\SubItems;
use App\Http\Resources\SubItemCollection; 
use Illuminate\Support\Facades\Auth;

class ItemCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) use ($request) {
            
            //$subItem = ItemDetails::with('subItem')->where('item_id',$item->id)->where('user_id', Auth::user()->id)->get();
               
           // $subItem = SubItems::where('item_id',$item->id)->where('user_id', Auth::user()->id)->get();

            return [
                'Sub_Item_id' => $item->id,
                'SubItem_name' => $item->name,
               // 'SubItem' => new SubItemCollection($subItem)
            ];    
        });
    }
    public function with($request)
    {
        return [
            'success' => true,
        ];
    }
}