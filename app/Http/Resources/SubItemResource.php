<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\Resource;

class SubItemResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return $this->collection->map(function ($subitem) use ($request) {
            // dd('ok');
                 return [
                    //'subitem_id' => $subitem->getKey(),
                    //'SubItem' => $subitem["sub_item"],
                    'sub_item_name' => $this->name,
                ];    
            // });
    }
     public function with($request)
    {
        return [
            'success' => true,
        ];
    }  
}
