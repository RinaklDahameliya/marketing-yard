<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Models\Loading; 
use App\Models\ItemDetails; 
use Illuminate\Support\Facades\DB;
use App\Http\Resources\LoderCollection;

class LoadedViewDetailsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($item) use ($request) {

        // $agent = ItemDetails::where('id',$item->item_details_id)->groupBy('id')->get();
    
        if(isset($request->date) && !empty($request->date)){
            $loders = $item->loaders()->whereDate('date', $request->date)->get();
        }else{
            $loders = $item->loaders()->get();
        }

        return [
            'commission_agent,' => $item->commission_agent,
            'loders' => new LoderCollection($loders)

            // 'id' => $item->getKey(),
            // 'item_details_id' => $item->item_details_id,
            // 'quantity' => $item->quantity,
            // 'gate pass' => $item->gate_pass
            // 'car number' => $item->car_number,
            // 'driver name' => $item->driver_name,
            // 'commission_agent' => new AgentCollection($agent),
            //'commission_agent' => $item->commission_agent
                ];   
        });
    }
    public function with($request)
    {
        return [
            'success' => true,
        ];
    }   
}